function Scope () {
    this.$$watchers = [];
}

Scope.prototype.$watch = function (scope, listener) {
    var watcher = {
        watcherFn: function(){return scope.name},
        listener: listener
    };
    this.$$watchers.push(watcher);
};

Scope.prototype.$digest = function () {
    var self = this;
    this.$$watchers.forEach(function(watcher) {
        var newVal = watcher.watcherFn(self);
        var oldVal = watcher.oldData;
        if (newVal !== oldVal) {
            watcher.listener(newVal, oldVal, self);
        }
        watcher.oldData = newVal;
    });
};

var scope = new Scope();
scope.name = 'Narek';
scope.counter = 0;
scope.$watch(scope, function (newVal, oldVal, scope) {
    scope.counter ++ ;
});

console.log(scope.counter);

scope.$digest();
console.log(scope.counter);

scope.$digest();
console.log(scope.counter);

scope.name = 'david';
scope.$digest();
console.log(scope.counter);
scope.name = 'Rafael';
scope.$digest();
console.log(scope.counter);